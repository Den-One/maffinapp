#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Menu.cpp"

#include <QPushButton>
#include <QBoxLayout>
#include <QScrollArea>
#include <QLayout>
#include <QPixmap>

#include <QObject>
#include <QToolButton>
#include <QGridLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(360, 640));
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    ui->startButton->setFlat(true);

    // make order
    ui->makeOrder_1->hide();
    ui->coffeeImg_1->hide();
    ui->foodImg_1->hide();
    ui->showDrinks_1->hide();
    ui->showFood_1->hide();
    ui->goMainMenu->hide();
    ui->goFinishOrder->hide();
    ui->scrollArea->hide();
    ui->productName_1->hide();
    ui->productNumber_1->hide();
    ui->muffinImg_2->hide();
    ui->myOrder_2->hide();
    ui->noButton_2->hide();
    ui->yesButton_2->hide();
    ui->totalSum_2->hide();
    ui->startNewOrder_3->hide();
    ui->finishImg_3->hide();

    // initialize deal-window
    dealWin = new DealWindow();
    connect(dealWin, &DealWindow::showDealWindow, this, &MainWindow::show);
    connect(dealWin, &DealWindow::signalGetOrderInformation, this, &MainWindow::slotToTakeOrderInformation);

    // create pictures
    QPixmap muffinLogo(":/pictures/pictures/MuffMainLogo.jpg");
    QPixmap yellowColor(":/pictures/pictures/mainTopColor.jpg");
    QPixmap drinksImg(":/pictures/pictures/DrinksButton.jpg");
    QPixmap foodImg(":/pictures/pictures/foodButton.jpg");
    QPixmap finishImg(":/pictures/pictures/orderIsDone.jpg");
    ui->muffinMainImg->setPixmap(muffinLogo);
    ui->muffinImg_2->setPixmap(muffinLogo);
    ui->topYellowCover->setPixmap(yellowColor);
    ui->coffeeImg_1->setPixmap(drinksImg);
    ui->foodImg_1->setPixmap(foodImg);
    ui->finishImg_3->setPixmap(finishImg);

}

MainWindow::~MainWindow()
{
    delete ui;
}

Vector <Section*> createDataBase()
{
    Vector<Section*>sections;

    Section* section = new Section("Еда", 0);

    Type* type = new Type("На завтрак", 0);
    type->addNewFood("Сырники с рикоттой", 150);
    type->addNewFood("Запеканка без сахара", 150);
    type->addNewFood("Шоколадная запеканка", 170);
    type->addNewFood("Гранола на банановом молоке", 130);
    type->addNewFood("Рисовая каша на кокосом", 130);
    type->addNewFood("Кукурузная каша на миндальном", 130);
    section->addType(type);

    type = new Type("Салаты", 1);
    type->addNewFood("С курицей и брустникой", 170);
    type->addNewFood("С сёмгой и брынзой", 150);
    section->addType(type);

    type = new Type("Круассан-сэндвич", 2);
    type->addNewFood("С пряной бужениной", 290);
    type->addNewFood("С лососем", 290);
    type->addNewFood("С вишней и кокосом", 260);
    type->addNewFood("С клубникой и маракуйей", 260);
    section->addType(type);

    type = new Type("Сэндвичи", 3);
    type->addNewFood("Сэндвич с курицей", 170);
    type->addNewFood("Сэндвич с красной рыбой", 170);
    type->addNewFood("Сэндвич ветчина-сыр", 170);
    section->addType(type);

    type = new Type("Супы", 4);
    type->addNewFood("Крем-суп сырный", 130);
    type->addNewFood("Крем-суп грибной", 120);
    section->addType(type);
    sections.push_back(section);

    section = new Section("Напитки", 1);
    type = new Type("Чёрный кофе", 0);
    type->addNewFood("Эспрессо", 100, 60); //
    type->addNewFood("Лунго", 130, 250);
    type->addNewFood("Американо", 100, 250);
    type->addNewFood("Фильтр-кофе", 100, 300);
    type->addNewFood("Воронка", 150, 250);
    type->addNewFood("Кемекс", 175, 500);
    section->addType(type);

    type = new Type("Кофе с молоком", 1);
    type->addNewFood("Капучино", 120, 150, 200, 300);
    type->addNewFood("Латте", 150, 170, 300, 400);
    type->addNewFood("Раф", 170, 200, 300, 400);
    type->addNewFood("Мокко", 180, 300);
    type->addNewFood("Флэт-Уайт", 150, 180);
    type->addNewFood("Какао", 150, 180, 300, 400);
    section->addType(type);

    type = new Type("Авторское кофе", 2);
    type->addNewFood("Латте белый шоколад", 180, 220, 300, 400);
    type->addNewFood("Арахисовый кофе", 180, 220, 300, 400);
    type->addNewFood("Сырный кофе", 180, 220, 300, 400);
    type->addNewFood("Банановый кофе", 180, 220, 300, 400);
    type->addNewFood("Кофе с халвой", 160, 180, 300, 400);
    section->addType(type);

    type = new Type("Чайная коллекция", 3);
    type->addNewFood("Облепиха-имбрить", 160, 200, 300, 500);
    type->addNewFood("Клюква-чабрец", 160, 200, 300, 500);
    type->addNewFood("Малина-имбирь", 160, 200, 300, 500);
    section->addType(type);

    type = new Type("Рафы", 4);
    type->addNewFood("Малиновый", 200, 300);
    type->addNewFood("Дыня-кокос", 200, 300);
    type->addNewFood("Солёная карамель", 200, 300);
    type->addNewFood("Апельсиновый пломбир", 200, 300);
    section->addType(type);

    type = new Type("Чай-латте", 5);
    type->addNewFood("Жасмин-имбирь-мята", 150, 300);
    type->addNewFood("Пуэр-клубника-бадьян", 150, 300);
    section->addType(type);

    type = new Type("Милкшейки", 6);
    type->addNewFood("Белый шоколад", 180, 220, 300, 400);
    type->addNewFood("Клубника-банан", 180, 220, 300, 400);
    type->addNewFood("Дыня-кокос", 180, 220, 300, 400);
    type->addNewFood("Арахис-банан", 180, 220, 300, 400);
    section->addType(type);

    type = new Type("Лимонады", 7);
    type->addNewFood("Матча-фейхоа", 150, 170, 300, 500);
    type->addNewFood("Клубника-манго", 150, 170, 300, 500);
    type->addNewFood("Груша-розмарин", 150, 170, 300, 500);
    section->addType(type);

    type = new Type("Смузи", 8);
    type->addNewFood("Клубника-банан", 180, 220, 300, 500);
    type->addNewFood("Тропик", 180, 220, 300, 500);
    type->addNewFood("Вишня-лайм", 180, 220, 300, 500);
    section->addType(type);

    type = new Type("Холодный кофе", 9);
    type->addNewFood("Глясе", 170, 220, 300, 500);
    type->addNewFood("Бамбл", 160, 300);
    type->addNewFood("Айс-латте", 150, 180, 300, 500);
    section->addType(type);
    sections.push_back(section);

    return sections;
}

Vector <Section*> sections = createDataBase();
QVector <Order*> orders;

void MainWindow::on_startButton_clicked()
{
    ui->startButton->hide();
    ui->topYellowCover->hide();
    ui->buttonInstruction->hide();
    ui->muffinMainImg->hide();
    ui->startNewOrder_3->hide();

    // make order
    ui->makeOrder_1->show();
    ui->coffeeImg_1->show();
    ui->foodImg_1->show();
    ui->showDrinks_1->show();
    ui->showFood_1->show();
    ui->goMainMenu->show();
    ui->goFinishOrder->show();
    //ui->scrollArea->move(19, 275);

    float number = ui->productNumber_1->text().split(" ")[0].toFloat();
    if (number > 0) {
        ui->productNumber_1->show();
    }

    ui->scrollArea->setWidget(makeWidgetForSection(1));
    ui->scrollArea->show();

    ui->productName_1->setText("НАПИТКИ");
    ui->productName_1->setAlignment(Qt::AlignCenter);
    ui->productName_1->show();
}

void MainWindow::on_goMainMenu_clicked()
{
    orders.clear();
    orders.shrink_to_fit();

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber = 0;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);

    ui->makeOrder_1->hide();
    ui->coffeeImg_1->hide();
    ui->foodImg_1->hide();
    ui->showDrinks_1->hide();
    ui->showFood_1->hide();
    ui->goMainMenu->hide();
    ui->goFinishOrder->hide();
    ui->scrollArea->hide();
    ui->productName_1->hide();
    ui->productNumber_1->hide();
    ui->startNewOrder_3->hide();

    ui->startButton->show();
    ui->topYellowCover->show();
    ui->buttonInstruction->show();
    ui->muffinMainImg->show();
}

int typePosition(-1);
int sectionPosition(1);

QWidget* MainWindow::makeWidgetForSection(int sectionPosition) {

    Vector <QPushButton*> coffeeButtons;

    const QSize btnSize = QSize(284, 41);
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setSpacing(14);
    for (int i = 0; i < sections[sectionPosition]->getVectorSize(); ++i) {
        string textOnButton = sections[sectionPosition]->getTypeNameOfSection(i);
        QPushButton *button = new QPushButton(textOnButton.c_str());
        coffeeButtons.push_back(button);
        //coffeeButtons[i] = new QPushButton(textOnButton.c_str());
        coffeeButtons[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        coffeeButtons[i]->setFixedSize(btnSize);

        switch (i) {
        case 0: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType0_clicked); break;
        case 1: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType1_clicked); break;
        case 2: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType2_clicked); break;
        case 3: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType3_clicked); break;
        case 4: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType4_clicked); break;
        case 5: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType5_clicked); break;
        case 6: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType6_clicked); break;
        case 7: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType7_clicked); break;
        case 8: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType8_clicked); break;
        case 9: connect(coffeeButtons[i], &QPushButton::clicked, this, &MainWindow::on_foodType9_clicked); break;
        }
        layout->addWidget(coffeeButtons[i]);
    }
    QWidget* widget = new QWidget();
    widget->setLayout(layout);

    return widget;
}

void MainWindow::on_showFood_1_clicked()
{
    sectionPosition = 0;
    ui->scrollArea->setWidget(makeWidgetForSection(sectionPosition));
    ui->productName_1->setText("ЕДА И ДЕСЕРТЫ");
    ui->productName_1->setAlignment(Qt::AlignCenter);
    ui->productName_1->show();
}

void MainWindow::on_showDrinks_1_clicked()
{
    sectionPosition = 1;
    ui->scrollArea->setWidget(makeWidgetForSection(sectionPosition));
    ui->productName_1->setText("НАПИТКИ");
    ui->productName_1->setAlignment(Qt::AlignCenter);
    ui->productName_1->show();
}

QWidget* MainWindow::makeWidgetForType(int typePosition) {

    Vector <QPushButton*> foodButtons;
    const QSize btnSize = QSize(284, 41);
    QVBoxLayout *layout = new QVBoxLayout();
    layout->setSpacing(14);
    for (int i = 0; i < sections[sectionPosition]->getFoodLength(typePosition); ++i) {
        string textOnButton = sections[sectionPosition]->getFoodName(typePosition, i);
        QPushButton *button = new QPushButton(textOnButton.c_str());
        foodButtons.push_back(button);
        foodButtons[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        foodButtons[i]->setFixedSize(btnSize);

        switch (i) {
            case 0: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food0_clicked); break;
            case 1: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food1_clicked); break;
            case 2: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food2_clicked); break;
            case 3: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food3_clicked); break;
            case 4: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food4_clicked); break;
            case 5: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food5_clicked); break;
            case 6: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food6_clicked); break;
            case 7: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food7_clicked); break;
            case 8: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food8_clicked); break;
            case 9: connect(foodButtons[i], &QPushButton::clicked, this, &MainWindow::on_food9_clicked); break;
        }

        connect(this, &MainWindow::signalSetPrice, dealWin, &DealWindow::on_priceButton_clicked); // send price
        connect(this, &MainWindow::signalSetName, dealWin, &DealWindow::on_nameLabel_set);

        Drink* drink;
        if (drink = dynamic_cast<Drink*>(sections[sectionPosition]->getFoodPointer(typePosition, i))) {
            if (drink->getWeightMin() != 0)
                connect(this, &MainWindow::signalSetWeightMin, dealWin, &DealWindow::on_weightMin_set);

            if (drink->getWeightMax() != 0) {
                connect(this, &MainWindow::signalSetPriceMax, dealWin, &DealWindow::on_priceButtonMax_clicked);
                connect(this, &MainWindow::signalSetWeightMax, dealWin, &DealWindow::on_weightMax_set);
            }
        }

        layout->addWidget(foodButtons[i]);
    }
    QWidget* widget = new QWidget();
    widget->setLayout(layout);

    ui->productName_1->setText(sections[sectionPosition]->getTypeNameOfSection(typePosition).c_str());
    ui->productName_1->setAlignment(Qt::AlignCenter);

    return widget;
}

void MainWindow::on_foodType0_clicked() {

    typePosition = 0;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType1_clicked() {
    typePosition = 1;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType2_clicked() {
    typePosition = 2;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType3_clicked() {
    typePosition = 3;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType4_clicked() {
    typePosition = 4;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType5_clicked() {
    typePosition = 5;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType6_clicked() {
    typePosition = 6;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType7_clicked() {
    typePosition = 7;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType8_clicked() {
    typePosition = 8;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::on_foodType9_clicked() {
    typePosition = 9;
    ui->scrollArea->setWidget(makeWidgetForType(typePosition));
}

void MainWindow::openAndInisializeOrderWindow(int foodPosition) {
    connect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
    emit signalSetPrice(sections[sectionPosition]->getFoodPriceMin(typePosition, foodPosition));
    emit signalSetName(sections[sectionPosition]->getFoodName(typePosition, foodPosition));

    Drink* drink;
    if (drink = dynamic_cast<Drink*>(sections[sectionPosition]->getFoodPointer(typePosition, foodPosition))) {
        if (drink->getWeightMin() != 0)
            emit signalSetWeightMin(sections[sectionPosition]->getWeightMin(typePosition, foodPosition));

        if (drink->getWeightMax() != 0) {
            emit signalSetPriceMax(sections[sectionPosition]->getFoodPriceMax(typePosition, foodPosition));
            emit signalSetWeightMax(sections[sectionPosition]->getWeightMax(typePosition, foodPosition));
        }
    }

    dealWin->show();
}

void MainWindow::on_food0_clicked() {
    int foodPosition = 0;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food1_clicked() {
    int foodPosition = 1;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food2_clicked() {
    int foodPosition = 2;
        openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food3_clicked() {
    int foodPosition = 3;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food4_clicked() {
    int foodPosition = 4;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food5_clicked() {
    int foodPosition = 5;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food6_clicked() {
    int foodPosition = 6;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food7_clicked() {
    int foodPosition = 7;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food8_clicked() {
    int foodPosition = 8;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::on_food9_clicked() {
    int foodPosition = 9;
    openAndInisializeOrderWindow(foodPosition);
}

void MainWindow::slotToTakeProductNumber(QString productNumber) {
    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber++;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    ui->productNumber_1->show();
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
    dealWin->close();
}

void MainWindow::slotToTakeOrderInformation(QString name, float price, float amount) {
    Order *newOrder = new Order(name.toStdString(), price, amount);
    orders.push_back(newOrder);
}

QVector <QLabel*> orderStrings;
QVector <QPushButton*> closeButtons;

void MainWindow::on_goFinishOrder_clicked()
{
    if (orders.size() > 0) {
        // clean up old elements
        ui->coffeeImg_1->hide();
        ui->foodImg_1->hide();
        ui->showDrinks_1->hide();
        ui->showFood_1->hide();
        ui->goMainMenu->hide();
        ui->goFinishOrder->hide();
        ui->productNumber_1->hide();
        ui->makeOrder_1->hide();

        //show new elements
        ui->muffinImg_2->show();
        ui->myOrder_2->show();
        ui->noButton_2->show();
        ui->yesButton_2->show();

        const QSize lineSize = QSize(247, 41); // 284, 41
        QGridLayout *layout = new QGridLayout();
        layout->setColumnStretch(0, 0);
        layout->setColumnStretch(1, 500);
        layout->setSpacing(14);

        int totalPrice = 0;
        for (int i = 0; i < orders.size(); ++i) {
            QString textOnLabel = orders[i]->getName().c_str();
            textOnLabel += " ";
            textOnLabel += QString::number(orders[i]->getPrice());
            textOnLabel += " руб. ";
            textOnLabel += QString::number(orders[i]->getAmount());
            textOnLabel += " шт.";
            //"Латте 300 руб. x3";
            totalPrice += orders[i]->getPrice();
            QLabel *label = new QLabel(textOnLabel);
            orderStrings.push_back(label);
            orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
            orderStrings[i]->setAlignment(Qt::AlignCenter);
            orderStrings[i]->setFixedSize(lineSize);
            layout->addWidget(orderStrings[i], i, 1);

            QPushButton *deleteButton = new QPushButton();
            closeButtons.push_back(deleteButton);
            closeButtons[i]->setFixedSize(23, 23);
            closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
            closeButtons[i]->setText("x");
            QFont font  = closeButtons[i]->font();
            font.setPointSize(13);
            closeButtons[i]->setFont(font);

            switch (i) {
                case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
                case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
                case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
                case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
                case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
                case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
                case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
                case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
                case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
                case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
            }

            layout->addWidget(closeButtons[i], i, 0);
        }

        QString textTotalOrder = "К оплате: ";
        textTotalOrder += QString::number(totalPrice);
        textTotalOrder += " руб.";
        ui->totalSum_2->setText(textTotalOrder);
        ui->totalSum_2->setAlignment(Qt::AlignCenter);
        ui->totalSum_2->show();

        QWidget* widget = new QWidget();
        widget->setLayout(layout);
        ui->scrollArea->setWidget(widget);
        ui->scrollArea->move(19, 230); //260


        ui->productName_1->setText(" ЗАКАЗ СОСТАВЛЕН ВЕРНО ?");
    }
}


void MainWindow::on_noButton_2_clicked()
{
    closeButtons.clear();
    closeButtons.shrink_to_fit();
    orderStrings.clear();
    orderStrings.shrink_to_fit();

    //Hide elements of 2 sceene
    ui->muffinImg_2->hide();
    ui->myOrder_2->hide();
    ui->noButton_2->hide();
    ui->yesButton_2->hide();
    ui->totalSum_2->hide();

    // make order
    ui->makeOrder_1->show();
    ui->coffeeImg_1->show();
    ui->foodImg_1->show();
    ui->showDrinks_1->show();
    ui->showFood_1->show();
    ui->goMainMenu->show();
    ui->goFinishOrder->show();

    float number = ui->productNumber_1->text().split(" ")[0].toFloat();
    if (number > 0) {
        ui->productNumber_1->show();
    }

    ui->scrollArea->setWidget(makeWidgetForSection(1));
    ui->scrollArea->move(19, 275);
    ui->scrollArea->show();

    ui->productName_1->setText("НАПИТКИ");
    ui->productName_1->setAlignment(Qt::AlignCenter);
    ui->productName_1->show();
}

void MainWindow::on_deletePosition0_clicked() {
    int positionNumberToDelete = 0;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition1_clicked() {
    int positionNumberToDelete = 1;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition2_clicked() {
    int positionNumberToDelete = 2;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition3_clicked() {
    int positionNumberToDelete = 3;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition4_clicked() {
    int positionNumberToDelete = 4;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition5_clicked() {
    int positionNumberToDelete = 5;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition6_clicked() {
    int positionNumberToDelete = 6;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition7_clicked() {
    int positionNumberToDelete = 7;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition8_clicked() {
    int positionNumberToDelete = 8;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}
void MainWindow::on_deletePosition9_clicked() {
    int positionNumberToDelete = 9;

    for (int i = 0; i < orders.size(); ++i) {
        switch (i) {
            case 0: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: disconnect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }
    }

    closeButtons.erase(closeButtons.begin() + positionNumberToDelete);
    orderStrings.erase(orderStrings.begin() + positionNumberToDelete);
    orders.erase(orders.begin() + positionNumberToDelete);

    const QSize lineSize = QSize(247, 41); // 284, 41
    QGridLayout *layout = new QGridLayout();
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 500);
    layout->setSpacing(14);

    int totalPrice = 0;
    for (int i = 0; i < orders.size(); ++i) {
        QString textOnLabel = orders[i]->getName().c_str();
        textOnLabel += " ";
        textOnLabel += QString::number(orders[i]->getPrice());
        textOnLabel += " руб. ";
        textOnLabel += QString::number(orders[i]->getAmount());
        textOnLabel += " шт.";
        //"Латте 300 руб. x3";
        totalPrice += orders[i]->getPrice();
        QLabel *label = new QLabel(textOnLabel);
        orderStrings.push_back(label);
        orderStrings[i]->setStyleSheet("background-color:rgb(255, 255, 255)");
        orderStrings[i]->setAlignment(Qt::AlignCenter);
        orderStrings[i]->setFixedSize(lineSize);
        layout->addWidget(orderStrings[i], i, 1);

        QPushButton *deleteButton = new QPushButton();
        closeButtons.push_back(deleteButton);
        closeButtons[i]->setFixedSize(23, 23);
        closeButtons[i]->setStyleSheet("background-color:rgb(248, 73, 45)");
        closeButtons[i]->setText("x");
        QFont font  = closeButtons[i]->font();
        font.setPointSize(13);
        closeButtons[i]->setFont(font);

        switch (i) {
            case 0: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition0_clicked); break;
            case 1: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition1_clicked); break;
            case 2: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition2_clicked); break;
            case 3: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition3_clicked); break;
            case 4: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition4_clicked); break;
            case 5: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition5_clicked); break;
            case 6: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition6_clicked); break;
            case 7: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition7_clicked); break;
            case 8: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition8_clicked); break;
            case 9: connect(closeButtons[i], &QPushButton::clicked, this, &MainWindow::on_deletePosition9_clicked); break;
        }

        layout->addWidget(closeButtons[i], i, 0);
    }

    QString textTotalOrder = "К оплате: ";
    textTotalOrder += QString::number(totalPrice);
    textTotalOrder += " руб.";
    ui->totalSum_2->setText(textTotalOrder);
    ui->totalSum_2->setAlignment(Qt::AlignCenter);
    ui->totalSum_2->show();

    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    ui->scrollArea->setWidget(widget);

    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber--;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->productNumber_1->setAlignment(Qt::AlignRight);
    disconnect(dealWin, &DealWindow::signalSetNumberInBasket, this, &MainWindow::slotToTakeProductNumber);
}

void MainWindow::on_yesButton_2_clicked()
{
    if (orders.size() > 0) {
        ui->myOrder_2->setText("ЗАКАЗ ОПЛАЧЕН");
        ui->myOrder_2->setAlignment(Qt::AlignCenter);
        ui->scrollArea->hide();
        ui->productName_1->setText("ГОТОВЫ СДЕЛАТЬ НОВЫЙ ЗАКАЗ ?");
        ui->productName_1->setAlignment(Qt::AlignCenter);
        ui->totalSum_2->hide();
        ui->yesButton_2->hide();
        ui->noButton_2->hide();

        ui->startNewOrder_3->show();
        ui->finishImg_3->show();
    }
}

void MainWindow::on_startNewOrder_3_clicked()
{
    orders.clear();
    orders.shrink_to_fit();
    orderStrings.clear();
    orderStrings.shrink_to_fit();
    closeButtons.clear();
    closeButtons.shrink_to_fit();
    int oldNumber = ui->productNumber_1->text().split(" ")[0].toFloat();
    oldNumber = 0;
    ui->productNumber_1->setText(QString::number(oldNumber) + " в заказе");
    ui->scrollArea->move(19, 275);

    ui->startButton->show();
    ui->topYellowCover->show();
    ui->buttonInstruction->show();
    ui->muffinMainImg->show();
    ui->startNewOrder_3->show();

    ui->finishImg_3->hide();
    ui->muffinImg_2->hide();
    ui->startNewOrder_3->hide();
    ui->productName_1->hide();
    ui->myOrder_2->hide();
}
