#include <iostream>
#include <string>
#include <QVector>
#include "Vector.h"
using namespace std;

class Food
{
protected:
    string foodName;
    short priceInRublesMin;
    short foodId;

public:
    Food(string name, short price, short id)
        : foodName(name), priceInRublesMin(price), foodId(id) {}

    virtual string getName() const { return foodName; }
    virtual float getPrice() const { return priceInRublesMin; }
};

class Drink: public Food
{
private:
    short priceInRublesMax;

    short weightInGramsMin;
    short weightInGramsMax;

public:
    Drink(string name, short price, short id, short weightMin)
        : Food(name, price, id), weightInGramsMin(weightMin)
    {
        weightInGramsMax = 0;
        priceInRublesMax = 0;
    }

    Drink(string name, short priceMin, short priceMax, short id,
        short weightMin, short weightMax)
        : Food(name, priceMin, id), weightInGramsMin(weightMin),
        weightInGramsMax(weightMax), priceInRublesMax(priceMax) {}

    virtual string getName() const { return foodName; }
    virtual float getPrice() const { return priceInRublesMin; }

    float getPriceMax() const { return priceInRublesMax; }
    float getWeightMin() const { return weightInGramsMin; }
    float getWeightMax() const { return weightInGramsMax; }
};

//template <typename T>
class ConditionGetter {
private:
    virtual string getName() const = 0; // Type - getName() const
    virtual short getId() const = 0; //getTypeId()
    virtual short getVectorSize() const = 0; // Type = getFoodLength()
    //virtual T getElement(short i) const = 0; // string getFood(short i)
};

class Type: private ConditionGetter {
private:
    string typeName;
    short typeId;
    Vector <Food*> foods;

    short foodIdGenerator = 0;

public:
    Type(string name, short id) : typeName(name), typeId(id) {}

    void addNewFood(string foodName, int foodPrice) {
        foods.push_back(new Food(foodName, foodPrice, foodIdGenerator));
        foodIdGenerator++;
    }

    void addNewFood(string name, short price, short weight) {
        foods.push_back(new Drink(name, price, foodIdGenerator, weight));
        foodIdGenerator++;
    }

    void addNewFood(string name, short priceMin, short priceMax, short weightMin, short weightMax) {
        foods.push_back(new Drink(name, priceMin, priceMax, foodIdGenerator, weightMin, weightMax));
        foodIdGenerator++;
    }

    virtual string getName() const { return typeName; }
    virtual short getId() const { return typeId; }
    virtual short getVectorSize() const { return foods.size(); }

    string getFood(short i) { return foods[i]->getName(); }
    float getPriceMin(short i) { return foods[i]->getPrice(); }

    short getWeightMin(short i) {
        Drink* drink;
        drink = dynamic_cast<Drink*> (foods[i]);
        return drink->getWeightMin();
    }

    short getWeightMax(short i) {
        Drink* drink;
        drink = dynamic_cast<Drink*> (foods[i]);
        return drink->getWeightMax();
    }

    short getPriceMax(short i) {
        Drink* drink;
        drink = dynamic_cast<Drink*> (foods[i]);
        return drink->getPriceMax();
    }

    Food* getPointer(short i) {
        Food* f = foods[i];
        return f;
    }
};

class Section: private ConditionGetter {
private:
    string sectionName;
    short sectionId;
    Vector <Type*> types;

    short typeIdGenerator = 0;

public:
    Section(string name, short id) : sectionName(name), sectionId(id) {}

    void addType(Type* type) { types.push_back(type); }

    virtual string getName() const { return sectionName; }
    virtual short getId() const { return typeIdGenerator; }
    virtual short getVectorSize() const { return types.size(); }

    string getTypeNameOfSection(short typeId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getName();
        }
        return "";
    }

    short getFoodLength(short typeId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getVectorSize();
        }
        return 0;
    }

    string getFoodName(short typeId, short foodId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getFood(foodId);
        }
        return "";
    }

    float getFoodPriceMin(short typeId, short foodId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getPriceMin(foodId);
        }
        return 0;
    }

    float getFoodPriceMax(short typeId, short foodId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getPriceMax(foodId);
        }
        return 0;
    }

    float getWeightMin(short typeId, short foodId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getWeightMin(foodId);
        }

        return 0;
    }

    float getWeightMax(short typeId, short foodId) {
        for (const auto& type : types) {
            if (type->getId() == typeId)
                return type->getWeightMax(foodId);
        }

        return 0;
    }

    Food* getFoodPointer(short typeId, short foodId) {
        Food* t = types[typeId]->getPointer(foodId);
        return t;
    }
};
