/********************************************************************************
** Form generated from reading UI file 'dealwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEALWINDOW_H
#define UI_DEALWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DealWindow
{
public:
    QLabel *coffeeImg;
    QLabel *positionName;
    QPushButton *minusButton;
    QPushButton *valueButton;
    QPushButton *plusButton;
    QPushButton *addButton;
    QPushButton *butVar1;
    QPushButton *butVar2;

    void setupUi(QDialog *DealWindow)
    {
        if (DealWindow->objectName().isEmpty())
            DealWindow->setObjectName(QString::fromUtf8("DealWindow"));
        DealWindow->resize(360, 310);
        DealWindow->setStyleSheet(QString::fromUtf8("background-color:rgb(252, 216, 40);"));
        coffeeImg = new QLabel(DealWindow);
        coffeeImg->setObjectName(QString::fromUtf8("coffeeImg"));
        coffeeImg->setGeometry(QRect(30, 70, 300, 170));
        coffeeImg->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/\320\241\320\275\320\270\320\274\320\276\320\2723.png")));
        positionName = new QLabel(DealWindow);
        positionName->setObjectName(QString::fromUtf8("positionName"));
        positionName->setGeometry(QRect(30, 20, 300, 40));
        QFont font;
        font.setFamilies({QString::fromUtf8("Verdana")});
        font.setPointSize(14);
        positionName->setFont(font);
        positionName->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        minusButton = new QPushButton(DealWindow);
        minusButton->setObjectName(QString::fromUtf8("minusButton"));
        minusButton->setGeometry(QRect(30, 250, 40, 40));
        minusButton->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        valueButton = new QPushButton(DealWindow);
        valueButton->setObjectName(QString::fromUtf8("valueButton"));
        valueButton->setGeometry(QRect(70, 250, 40, 40));
        valueButton->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        plusButton = new QPushButton(DealWindow);
        plusButton->setObjectName(QString::fromUtf8("plusButton"));
        plusButton->setGeometry(QRect(110, 250, 40, 40));
        plusButton->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        addButton = new QPushButton(DealWindow);
        addButton->setObjectName(QString::fromUtf8("addButton"));
        addButton->setGeometry(QRect(160, 250, 170, 40));
        addButton->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        butVar1 = new QPushButton(DealWindow);
        butVar1->setObjectName(QString::fromUtf8("butVar1"));
        butVar1->setGeometry(QRect(30, 250, 140, 40));
        butVar1->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        butVar2 = new QPushButton(DealWindow);
        butVar2->setObjectName(QString::fromUtf8("butVar2"));
        butVar2->setGeometry(QRect(190, 250, 140, 40));
        butVar2->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));

        retranslateUi(DealWindow);

        QMetaObject::connectSlotsByName(DealWindow);
    } // setupUi

    void retranslateUi(QDialog *DealWindow)
    {
        DealWindow->setWindowTitle(QCoreApplication::translate("DealWindow", "Dialog", nullptr));
        coffeeImg->setText(QString());
        positionName->setText(QCoreApplication::translate("DealWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>", nullptr));
        minusButton->setText(QCoreApplication::translate("DealWindow", "-", nullptr));
        valueButton->setText(QCoreApplication::translate("DealWindow", "1", nullptr));
        plusButton->setText(QCoreApplication::translate("DealWindow", "+", nullptr));
        addButton->setText(QString());
        butVar1->setText(QString());
        butVar2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DealWindow: public Ui_DealWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEALWINDOW_H
