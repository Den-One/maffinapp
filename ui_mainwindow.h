/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *muffinMainImg;
    QLabel *topYellowCover;
    QLabel *buttonInstruction;
    QPushButton *startButton;
    QLabel *makeOrder_1;
    QPushButton *showDrinks_1;
    QPushButton *showFood_1;
    QPushButton *goMainMenu;
    QPushButton *goFinishOrder;
    QLabel *coffeeImg_1;
    QLabel *foodImg_1;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QLabel *productName_1;
    QLabel *productNumber_1;
    QLabel *muffinImg_2;
    QLabel *myOrder_2;
    QPushButton *noButton_2;
    QPushButton *yesButton_2;
    QLabel *totalSum_2;
    QPushButton *startNewOrder_3;
    QLabel *finishImg_3;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(360, 640);
        MainWindow->setMinimumSize(QSize(360, 640));
        MainWindow->setMaximumSize(QSize(360, 640));
        MainWindow->setStyleSheet(QString::fromUtf8("background-color:rgb(252, 216, 40);"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        muffinMainImg = new QLabel(centralwidget);
        muffinMainImg->setObjectName(QString::fromUtf8("muffinMainImg"));
        muffinMainImg->setGeometry(QRect(-220, 70, 1051, 391));
        muffinMainImg->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/UCQi2j-P_2w \342\200\224 \320\272\320\276\320\277\320\270\321\217.jpg")));
        topYellowCover = new QLabel(centralwidget);
        topYellowCover->setObjectName(QString::fromUtf8("topYellowCover"));
        topYellowCover->setGeometry(QRect(-30, -110, 421, 281));
        topYellowCover->setStyleSheet(QString::fromUtf8("background-color:rgb(254, 220, 34);"));
        buttonInstruction = new QLabel(centralwidget);
        buttonInstruction->setObjectName(QString::fromUtf8("buttonInstruction"));
        buttonInstruction->setGeometry(QRect(-10, 540, 381, 101));
        QFont font;
        font.setFamilies({QString::fromUtf8("Verdana")});
        font.setPointSize(14);
        font.setBold(false);
        buttonInstruction->setFont(font);
        buttonInstruction->setTabletTracking(false);
        buttonInstruction->setStyleSheet(QString::fromUtf8("background-color:rgb(241, 206, 40);"));
        buttonInstruction->setWordWrap(false);
        startButton = new QPushButton(centralwidget);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        startButton->setGeometry(QRect(-20, -20, 401, 651));
        makeOrder_1 = new QLabel(centralwidget);
        makeOrder_1->setObjectName(QString::fromUtf8("makeOrder_1"));
        makeOrder_1->setGeometry(QRect(-6, 0, 371, 61));
        QFont font1;
        font1.setFamilies({QString::fromUtf8("Verdana")});
        font1.setPointSize(14);
        makeOrder_1->setFont(font1);
        showDrinks_1 = new QPushButton(centralwidget);
        showDrinks_1->setObjectName(QString::fromUtf8("showDrinks_1"));
        showDrinks_1->setGeometry(QRect(20, 210, 151, 31));
        QFont font2;
        font2.setFamilies({QString::fromUtf8("Verdana")});
        font2.setPointSize(9);
        showDrinks_1->setFont(font2);
        showDrinks_1->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        showFood_1 = new QPushButton(centralwidget);
        showFood_1->setObjectName(QString::fromUtf8("showFood_1"));
        showFood_1->setGeometry(QRect(190, 210, 151, 31));
        showFood_1->setFont(font2);
        showFood_1->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        goMainMenu = new QPushButton(centralwidget);
        goMainMenu->setObjectName(QString::fromUtf8("goMainMenu"));
        goMainMenu->setGeometry(QRect(20, 580, 151, 31));
        goMainMenu->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        goFinishOrder = new QPushButton(centralwidget);
        goFinishOrder->setObjectName(QString::fromUtf8("goFinishOrder"));
        goFinishOrder->setGeometry(QRect(189, 580, 151, 31));
        goFinishOrder->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        coffeeImg_1 = new QLabel(centralwidget);
        coffeeImg_1->setObjectName(QString::fromUtf8("coffeeImg_1"));
        coffeeImg_1->setGeometry(QRect(20, 60, 151, 151));
        QFont font3;
        font3.setPointSize(16);
        coffeeImg_1->setFont(font3);
        coffeeImg_1->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/cszFSVmkCXU2.jpg")));
        foodImg_1 = new QLabel(centralwidget);
        foodImg_1->setObjectName(QString::fromUtf8("foodImg_1"));
        foodImg_1->setGeometry(QRect(189, 60, 151, 151));
        foodImg_1->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/N2nQAwL1xOI.jpg")));
        scrollArea = new QScrollArea(centralwidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(19, 274, 321, 221));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 319, 219));
        scrollArea->setWidget(scrollAreaWidgetContents);
        productName_1 = new QLabel(centralwidget);
        productName_1->setObjectName(QString::fromUtf8("productName_1"));
        productName_1->setGeometry(QRect(0, 510, 361, 41));
        QFont font4;
        font4.setFamilies({QString::fromUtf8("Verdana")});
        font4.setPointSize(11);
        productName_1->setFont(font4);
        productName_1->setAutoFillBackground(false);
        productName_1->setStyleSheet(QString::fromUtf8("background-color:rgb(241, 206, 40);"));
        productNumber_1 = new QLabel(centralwidget);
        productNumber_1->setObjectName(QString::fromUtf8("productNumber_1"));
        productNumber_1->setGeometry(QRect(230, 559, 110, 20));
        QFont font5;
        font5.setFamilies({QString::fromUtf8("Verdana")});
        productNumber_1->setFont(font5);
        muffinImg_2 = new QLabel(centralwidget);
        muffinImg_2->setObjectName(QString::fromUtf8("muffinImg_2"));
        muffinImg_2->setGeometry(QRect(-220, -30, 971, 241));
        muffinImg_2->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/UCQi2j-P_2w \342\200\224 \320\272\320\276\320\277\320\270\321\217.jpg")));
        myOrder_2 = new QLabel(centralwidget);
        myOrder_2->setObjectName(QString::fromUtf8("myOrder_2"));
        myOrder_2->setGeometry(QRect(-6, 190, 371, 41));
        myOrder_2->setFont(font1);
        noButton_2 = new QPushButton(centralwidget);
        noButton_2->setObjectName(QString::fromUtf8("noButton_2"));
        noButton_2->setGeometry(QRect(30, 580, 140, 30));
        noButton_2->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        yesButton_2 = new QPushButton(centralwidget);
        yesButton_2->setObjectName(QString::fromUtf8("yesButton_2"));
        yesButton_2->setGeometry(QRect(190, 580, 140, 30));
        yesButton_2->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        totalSum_2 = new QLabel(centralwidget);
        totalSum_2->setObjectName(QString::fromUtf8("totalSum_2"));
        totalSum_2->setGeometry(QRect(59, 465, 241, 31));
        totalSum_2->setFont(font4);
        totalSum_2->setStyleSheet(QString::fromUtf8("background-color:rgb(241, 206, 40);"));
        startNewOrder_3 = new QPushButton(centralwidget);
        startNewOrder_3->setObjectName(QString::fromUtf8("startNewOrder_3"));
        startNewOrder_3->setGeometry(QRect(105, 580, 150, 30));
        startNewOrder_3->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 255, 255);"));
        finishImg_3 = new QLabel(centralwidget);
        finishImg_3->setObjectName(QString::fromUtf8("finishImg_3"));
        finishImg_3->setGeometry(QRect(30, 250, 300, 231));
        finishImg_3->setPixmap(QPixmap(QString::fromUtf8("../../Documents/\320\243\321\207\321\221\320\261\320\260/\320\230\320\234\320\237/\320\232\321\203\321\200\321\201\320\276\320\262\320\260\321\217/pictures/ZUTPLzNR8B84.jpg")));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 360, 25));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        muffinMainImg->setText(QString());
        topYellowCover->setText(QString());
        buttonInstruction->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\320\232\320\236\320\241\320\235\320\230\320\242\320\225\320\241\320\254 \320\255\320\232\320\240\320\220\320\235\320\220 \320\224\320\233\320\257 \320\242\320\236\320\223\320\236,</p><p align=\"center\">\320\247\320\242\320\236\320\221\320\253 \320\235\320\220\320\247\320\220\320\242\320\254</p></body></html>", nullptr));
        startButton->setText(QString());
        makeOrder_1->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\320\236\320\244\320\236\320\240\320\234\320\233\320\225\320\235\320\230\320\225 \320\227\320\220\320\232\320\220\320\227\320\220</p></body></html>", nullptr));
        showDrinks_1->setText(QCoreApplication::translate("MainWindow", "\320\235\320\220\320\237\320\230\320\242\320\232\320\230", nullptr));
        showFood_1->setText(QCoreApplication::translate("MainWindow", "\320\225\320\224\320\220 \320\230 \320\224\320\225\320\241\320\225\320\240\320\242\320\253", nullptr));
        goMainMenu->setText(QCoreApplication::translate("MainWindow", "\320\222\320\253\320\245\320\236\320\224", nullptr));
        goFinishOrder->setText(QCoreApplication::translate("MainWindow", "\320\236\320\244\320\236\320\240\320\234\320\230\320\242\320\254 \320\227\320\220\320\232\320\220\320\227", nullptr));
        coffeeImg_1->setText(QString());
        foodImg_1->setText(QString());
        productName_1->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\320\235\320\220\320\237\320\230\320\242\320\232\320\230</p></body></html>", nullptr));
        productNumber_1->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p align=\"right\">0 \320\262 \320\267\320\260\320\272\320\260\320\267\320\265</p></body></html>", nullptr));
        myOrder_2->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\320\234\320\236\320\231 \320\227\320\220\320\232\320\220\320\227</p></body></html>", nullptr));
        noButton_2->setText(QCoreApplication::translate("MainWindow", "\320\235\320\225\320\242", nullptr));
        yesButton_2->setText(QCoreApplication::translate("MainWindow", "\320\224\320\220", nullptr));
        totalSum_2->setText(QCoreApplication::translate("MainWindow", "\320\232 \320\276\320\277\320\273\320\260\321\202\320\265: ", nullptr));
        startNewOrder_3->setText(QCoreApplication::translate("MainWindow", "\320\224\320\220", nullptr));
        finishImg_3->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
