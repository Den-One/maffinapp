#include <iostream>
#include <string>
using std::string;

struct Order
{
private:
    float m_price;
    float m_amount;
    string m_name;

public:
    Order(string name, float price, float amount)
    : m_price(price), m_amount(amount), m_name(name) {}

    float getPrice() { return m_price; }
    float getAmount() { return m_amount; }
    string getName() { return m_name; }
};
