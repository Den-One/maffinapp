#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <dealwindow.h>
#include <iostream>
#include "order.cpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void signalSetName(std::string foodName);
    void signalSetPrice(float foodPrice);
    void signalSetPriceMax(float foodPriceMax);
    void signalSetWeightMin(float foodWeightMin);
    void signalSetWeightMax(float foodWeightMax);

public slots:
    // dialog with dealWindow
    void slotToTakeProductNumber(QString productNumber);
    void slotToTakeOrderInformation(QString name, float price, float amount);

private slots:
    void on_startButton_clicked();

    void on_goMainMenu_clicked();

    void on_showFood_1_clicked();

    void on_showDrinks_1_clicked();

    //void on_button_clicked();


    QWidget* makeWidgetForType(int typePosition);
    QWidget* makeWidgetForSection(int sectionPosition);

    void on_foodType0_clicked();
    void on_foodType1_clicked();
    void on_foodType2_clicked();
    void on_foodType3_clicked();
    void on_foodType4_clicked();
    void on_foodType5_clicked();
    void on_foodType6_clicked();
    void on_foodType7_clicked();
    void on_foodType8_clicked();
    void on_foodType9_clicked();

    // new signals for under buttons
    void openAndInisializeOrderWindow(int foodPosition);

    void on_food0_clicked();
    void on_food1_clicked();
    void on_food2_clicked();
    void on_food3_clicked();
    void on_food4_clicked();
    void on_food5_clicked();
    void on_food6_clicked();
    void on_food7_clicked();
    void on_food8_clicked();
    void on_food9_clicked();


    void on_goFinishOrder_clicked();

    void on_noButton_2_clicked();

    // delete 10 positions
    void on_deletePosition0_clicked();
    void on_deletePosition1_clicked();
    void on_deletePosition2_clicked();
    void on_deletePosition3_clicked();
    void on_deletePosition4_clicked();
    void on_deletePosition5_clicked();
    void on_deletePosition6_clicked();
    void on_deletePosition7_clicked();
    void on_deletePosition8_clicked();
    void on_deletePosition9_clicked();

    void on_yesButton_2_clicked();

    void on_startNewOrder_3_clicked();

private:
    Ui::MainWindow *ui;
    DealWindow *dealWin;
};
#endif // MAINWINDOW_H
