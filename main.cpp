#include "mainwindow.h"
#include <QApplication>
#include <QWidget>
#include "Menu.cpp"

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "RU");
    QApplication a(argc, argv);
    MainWindow w;
    DealWindow d;

    QWidget window;

    w.setWindowTitle(" Muffin Menu");
    QPixmap smallLogoImg(":/pictures/pictures/MuffSmallLogo.jpg");
    w.setWindowIcon(QIcon(smallLogoImg));

    w.show();
    return a.exec();
}
