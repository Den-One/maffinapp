#include "dealwindow.h"
#include "ui_dealwindow.h"
#include "order.cpp"

#include <iostream>
using namespace std;

DealWindow::DealWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DealWindow)
{
    this->move(587, 250);
    this->resize(360, 310);

    //this->setFixedSize(QSize(360, 310));
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);

    ui->setupUi(this);
    this->setWindowTitle(" Checkout");

    QPixmap latteImg(":/pictures/pictures/LatteOrder.png");
    QPixmap smallLogo(":/pictures/pictures/MuffSmallLogo.jpg");
    ui->coffeeImg->setPixmap(latteImg);

    this->setWindowIcon(QIcon(smallLogo));

    ui->butVar1->hide();
    ui->butVar2->hide();

}

DealWindow::~DealWindow()
{
    delete ui;
}

void DealWindow::on_priceButton_clicked(float foodPrice)
{
    ui->addButton->setText("Добавить " + QString::number(foodPrice) + " руб.");
    ui->valueButton->setText("1");
}

void DealWindow::on_nameLabel_set(std::string foodName)
{
    ui->positionName->setText(foodName.c_str());
    ui->positionName->setAlignment(Qt::AlignCenter);
}

void DealWindow::on_priceButtonMax_clicked(float foodPriceMax)
{
    ui->butVar1->setText("Добавить " + QString::number(foodPriceMax) + " руб.");
    ui->butVar1->move(0, 0);
    ui->butVar1->show();

    cout << "PriceMax: " << foodPriceMax << endl;

    // 360, 310
    //this->setFixedSize(QSize(360, 350));
    /*if (foodPriceMax > 2) {
        this->resize(360, 310);
    }
    else {
        this->resize(360, 350);
    }*/

}

void DealWindow::on_weightMin_set(float weightMin)
{
    cout << "WeightMin: " << weightMin << endl;
    if (weightMin <= 0) {
        this->resize(360, 310);
    }
    else
    {
        this->resize(360, 350);
    }

}

void DealWindow::on_weightMax_set(float weightMax)
{
    cout << "WeightMax: " << weightMax << endl;
    //ui->butVar

}

void DealWindow::on_plusButton_clicked()
{
    QString oldPrice = ui->addButton->text(); //
    int oldSumPrice = oldPrice.split(" ")[1].toInt();
    int priceOfOneProduct = oldSumPrice / ui->valueButton->text().toInt();

    int newPrice = oldSumPrice + priceOfOneProduct;
    ui->addButton->setText("Добавить " + QString::number(newPrice) + " руб.");

    ui->minusButton->setEnabled(true);

    QString curButtonNumber = ui->valueButton->text();
    float newNumber = curButtonNumber.toInt();
    if (newNumber < 50) {
        newNumber++;
        ui->valueButton->setText(QString::number(newNumber));
    }
    if (newNumber == 50){
        ui->plusButton->setEnabled(false);
    }
}


void DealWindow::on_minusButton_clicked()
{
    QString oldPrice = ui->addButton->text();
    int oldSumPrice = oldPrice.split(" ")[1].toInt();
    int priceOfOneProduct = oldSumPrice / ui->valueButton->text().toInt();

    int newPrice = oldSumPrice - priceOfOneProduct;
    ui->addButton->setText("Добавить " + QString::number(newPrice) + " руб.");

    ui->plusButton->setEnabled(true);

    QString curButtonNumber = ui->valueButton->text();
    float newNumber = curButtonNumber.toInt();
    if (newNumber > 1) {
        newNumber--;
        ui->valueButton->setText(QString::number(newNumber));
    }
    if (newNumber == 1) {
        ui->minusButton->setEnabled(false);
    }
}


void DealWindow::on_addButton_clicked()
{
    emit signalSetNumberInBasket(ui->valueButton->text());
    emit signalGetOrderInformation(ui->positionName->text(), // name
                                   ui->addButton->text().split(" ")[1].toFloat(), // price
                                   ui->valueButton->text().toFloat()); // amount

}
