#ifndef DEALWINDOW_H
#define DEALWINDOW_H

#include <QDialog>
#include <QMainWindow>

namespace Ui {
class DealWindow;
}

class DealWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DealWindow(QWidget *parent = nullptr);
    ~DealWindow();

signals:
    void showDealWindow();

    void signalSetNumberInBasket(QString productNumber);
    void signalGetOrderInformation(QString name, float price, float amount);

public slots:
    void on_priceButton_clicked(float foodPrice);
    void on_nameLabel_set(std::string foodName);
    void on_priceButtonMax_clicked(float foodPriceMax);
    void on_weightMin_set(float weightMin);
    void on_weightMax_set(float weightMax);

private slots:
    void on_plusButton_clicked();

    void on_minusButton_clicked();

    void on_addButton_clicked();

private:
    Ui::DealWindow *ui;
    //MainWindow *mainW;
};

#endif // DEALWINDOW_H
